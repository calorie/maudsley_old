PROJECT_ROOT = File.expand_path(File.dirname(__FILE__))
PROJECT_MAUDSLEY_ROOT = 'vendor/maudsley'
load "#{PROJECT_MAUDSLEY_ROOT}/lib/maudsley/rakefile.rb"

task :default => 'spec:all'
