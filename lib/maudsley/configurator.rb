require 'yaml'
require 'maudsley/constants'

class Configurator
  def self.load_config
    path = "#{PROJECT_ROOT}/#{PROJECT_FILE_NAME}"
    validate_path(path)
    return YAML.load(File.read(path))
  end

  private

  def self.validate_path(path)
    unless File.exist?(path)
      puts "Found no Maudsley project file (#{PROJECT_FILE_NAME})"
      raise
    end
  end
end
