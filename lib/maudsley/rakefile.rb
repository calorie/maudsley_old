MAUDSLEY_ROOT      = File.expand_path(File.dirname(__FILE__) + '/../..')
MAUDSLEY_LIB       = File.join(MAUDSLEY_ROOT, 'lib')
MAUDSLEY_TASKS     = File.join(MAUDSLEY_ROOT, 'tasks')
MAUDSLEY_VENDOR    = File.join(MAUDSLEY_ROOT, 'vendor')
MAUDSLEY_TEMPLATES = File.join(MAUDSLEY_ROOT, 'templates')
$LOAD_PATH.unshift(MAUDSLEY_LIB)

require 'maudsley/configurator'

# load config
@config = Configurator.load_config

# load tasks
Dir.glob("#{MAUDSLEY_TASKS}/*.rake") do |task_file|
  load task_file
end
