require 'fileutils'
require 'thor'

class Cli < Thor
  include Thor::Actions

  desc 'new PROJECT_NAME', 'new a new maudsley project'
  def new(name = 'new_project')
    maudsley = File.join(name, 'vendor', 'maudsley')
    src      = File.join(name, 'src')
    spec     = File.join(name, 'spec')
    support  = File.join(name, 'spec/support')
    build    = File.join(name, 'build')

    [maudsley, src, spec, support, build].each do |d|
      FileUtils.mkdir_p d
    end

    folders = %w{lib tasks templates}
    folders.map! do |f|
      { :src => f, :dst => File.join(maudsley, f) }
    end
    folders.each do |f|
      directory f[:src], f[:dst]
    end

    %w{ rakefile.rb project.yml }.each do |f|
      copy_file File.join('assets', f), File.join(name, f)
    end

    puts ''
    puts "Project '#{name}' created!"
    puts " - Execute 'rake -T' to view available test & build tasks"
    puts ''
  end

  desc 'example PROJECT_NAME', 'new specified example project'
  def example(name = 'example_project')
    invoke :new, [name]

    examples = File.expand_path(File.dirname(__FILE__) + '/../../examples')
    %w{src spec}.each do |dir|
      Dir.glob("#{examples}/#{dir}/*") do |f|
        FileUtils.cp f, File.join(name, dir)
      end
    end
  end
end
