require 'erb'
require 'maudsley/constants'

class GenerateRunner
  def generate
    header_files = []
    specs = []
    src_path = "#{PROJECT_ROOT}/#{SRC_ROOT_NAME}"
    spec_path = "#{PROJECT_ROOT}/#{TEST_ROOT_NAME}"
    Dir.glob("#{src_path}/*.h") do |header_file|
      header_files << File.basename(header_file)
    end
    Dir.glob("#{spec_path}/*_spec.c") do |spec|
      specs << File.basename(spec, '.c') unless /main_spec\.c$/ =~ spec
    end
    erb = ERB.new(IO.read("#{MAUDSLEY_TEMPLATES}/runner.erb"), nil, '%')
    File.open("#{spec_path}/main_spec.c", 'w') do |f|
      f.write(erb.result(binding))
    end
  end
end
