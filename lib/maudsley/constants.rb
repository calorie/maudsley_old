SRC_ROOT_NAME     ||= 'src'

TEST_ROOT_NAME    ||= 'spec'
TEST_TASK_ROOT    ||= TEST_ROOT_NAME + ':'
TEST_SYM          ||= TEST_ROOT_NAME.to_sym

PROJECT_FILE_NAME = 'project.yml'
RAKE_FILE_NAME    = 'rakefile.rb'
