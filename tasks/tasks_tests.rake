require 'maudsley/constants'
require 'maudsley/generate_runner'

namespace TEST_SYM do

  desc 'Run all unit tests.'
  task :all do
    config  = @config['mpispec']
    command = "mpispec -np #{config['processes']} #{config['options']}"
    GenerateRunner.new.generate
    Dir.chdir(PROJECT_ROOT) { |root| system(command) }
  end
end
