require 'fileutils'
require 'tmpdir'
require 'yaml'
require 'erb'

class GemDirLayout
  attr_reader :gem_dir_base_name
  attr_reader :install_dir

  def initialize(dir)
    @gem_dir_base_name = 'gems'
    @install_dir = dir
    @d = File.join @install_dir, @gem_dir_base_name
    FileUtils.mkdir_p @d
  end

  def bin
    File.join(@d, 'bin')
  end

  def lib
    File.join(@d, 'lib')
  end
end

class SystemContext
  class VerificationFailed < Exception; end
  class InvalidBackupEnv < Exception; end

  attr_reader :dir, :gem

  def initialize
    @dir = Dir.mktmpdir
    @gem = GemDirLayout.new(@dir)
  end

  def done!
    FileUtils.rm_rf(@dir)
  end

  def deploy_gem
    @git_repo = File.expand_path(File.join(File.dirname(__FILE__), '..'))
    tmp_root = File.expand_path(File.dirname(__FILE__) + '/../templates')
    erb = ERB.new(IO.read("#{tmp_root}/gemfile.erb"), nil, '%')

    File.open(File.join(@dir, 'Gemfile'), 'w') do |f|
      f.write(erb.result(binding))
    end

    Dir.chdir @dir do
      with_constrained_env do
        checks = ["bundle install --path #{@gem.install_dir}",
                  'bundle exec ruby -S maudsley 2>&1']
        checks.each do |c|
          `#{c}`
          raise VerificationFailed.new(c) unless $?.success?
        end
      end
    end
  end

  def with_context
    Dir.chdir @dir do |current_dir|
      with_constrained_env do
        ENV['RUBYLIB'] = @gem.lib
        ENV['RUBYPATH'] = @gem.bin

        yield
      end
    end
  end

  def backup_env
    # Force a deep clone. Hacktacular, but works.
    @_env = YAML.load(ENV.to_hash.to_yaml)
  end

  def reduce_env(destroy_keys = [])
    ENV.keys.each { |k| ENV.delete(k) if destroy_keys.include?(k) }
  end

  def constrain_env
    destroy_keys = %w{BUNDLE_GEMFILE BUNDLE_BIN_PATH RUBYOPT}
    reduce_env(destroy_keys)
  end

  def restore_env
    if @_env
      @_env.each_pair { |k, v| ENV[k] = v }
    else
      raise InvalidBackupEnv.new
    end
  end

  def with_constrained_env
    begin
      backup_env
      constrain_env
      yield
    ensure
      restore_env
    end
  end
end

module MaudsleyTestCases
  def can_create_projects
    projects = %w{
      src spec spec/support build
      vendor/maudsley/lib
      vendor/maudsley/tasks
      vendor/maudsley/templates
      rakefile.rb project.yml
    }
    can_create_dirs_or_files(projects)
  end

  def can_create_examples
    examples = %w{
      spec/cpi_spec.c
      spec/fabs_spec.c
      spec/strcmp_spec.c
      src/cpi.c
      src/cpi.h
    }
    can_create_dirs_or_files(examples)
  end

  def can_create_dirs_or_files(dirs_or_files)
    Dir.chdir @proj_name do
      dirs_or_files.each do |dof|
        expect(File.exists?(dof)).to be_true
      end
    end
  end
end
