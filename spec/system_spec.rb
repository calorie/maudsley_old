require 'spec_system_helper'

describe 'System' do
  include MaudsleyTestCases

  before :all do
    @c = SystemContext.new
    @c.deploy_gem
  end

  after :all do
    @c.done!
  end

  before { @proj_name = 'fake_project' }
  after { @c.with_context { FileUtils.rm_rf @proj_name } }

  context 'new command' do
    before do
      @c.with_context do
        `bundle exec ruby -S maudsley new #{@proj_name} 2>&1`
      end
    end

    it 'can create projects' do
      @c.with_context do
        can_create_projects
      end
    end
  end

  context 'example command' do
    before do
      @c.with_context do
        `bundle exec ruby -S maudsley example #{@proj_name} 2>&1`
      end
    end

    it 'can create projects' do
      @c.with_context do
        can_create_projects
      end
    end

    it 'can create example files' do
      @c.with_context do
        can_create_examples
      end
    end

    it 'should be testable' do
      @c.with_context do
        Dir.chdir @proj_name do
          output = `bundle exec ruby -S rake spec:all`
          expect(output).to match(/100%/)
        end
      end
    end
  end

end
