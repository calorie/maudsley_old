require 'simplecov'
SimpleCov.start do
  add_filter '/spec/'
end

$LOAD_PATH.unshift(File.join(File.dirname(__FILE__), '..', 'lib'))
$LOAD_PATH.unshift(File.dirname(__FILE__))

require 'rspec'
require 'maudsley'
Dir["#{File.dirname(__FILE__)}/support/**/*.rb"].each { |f| require f }

RSpec.configure do |config|
  config.before(:all) { clear_consts }
end

def clear_consts
  %w{
    PROJECT_ROOT
    PROJECT_MAUDSLEY_ROOT
    MAUDSLEY_ROOT
    MAUDSLEY_LIB
    MAUDSLEY_TASKS
    MAUDSLEY_VENDOR
    MAUDSLEY_TEMPLATES
  }.each do |c|
    const = c.to_sym
    Object.send(:remove_const, const) if Object.const_defined?(const)
  end
end
