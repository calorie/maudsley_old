require 'fileutils'
require 'tmpdir'
require 'spec_helper'
require 'spec_system_helper'

describe 'Cli' do
  include MaudsleyTestCases

  before :all do
    @tmp = Dir.mktmpdir
    Cli.source_root(File.dirname(__FILE__) + '/..')
  end

  after :all do
    FileUtils.rm_rf(@tmp)
  end

  describe 'new' do
    before :all do
      @proj_name = 'fake_new_project'
      Dir.chdir @tmp do
        Cli.start("new #{@proj_name}".split)
      end
    end

    it 'can create projects' do
      Dir.chdir @tmp do
        can_create_projects
      end
    end
  end

  describe 'example' do
    before :all do
      @proj_name = 'fake_example_project'
      Dir.chdir @tmp do
        Cli.start("example #{@proj_name}".split)
      end
    end

    it 'can create projects' do
      Dir.chdir @tmp do
        can_create_projects
      end
    end

    it 'can create example files' do
      Dir.chdir @tmp do
        can_create_examples
      end
    end
  end
end
