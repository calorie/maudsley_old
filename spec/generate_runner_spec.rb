require 'fileutils'
require 'tmpdir'
require 'spec_helper'
require 'maudsley/constants'

describe 'GenerateRunner' do
  before :all do
    PROJECT_ROOT       = Dir.mktmpdir
    MAUDSLEY_TEMPLATES = File.join(File.dirname(__FILE__), '..', 'templates')
    @examples_path     = File.join(File.dirname(__FILE__), '..', 'examples')
    [SRC_ROOT_NAME, TEST_ROOT_NAME].each do |d|
      FileUtils.mkdir_p File.join(PROJECT_ROOT, d)
      Dir.glob("#{@examples_path}/#{d}/*") do |f|
        FileUtils.cp f, File.join(PROJECT_ROOT, d)
      end
    end
  end

  after :all do
    FileUtils.rm_rf(PROJECT_ROOT)
  end

  describe 'generate' do
    before do
      GenerateRunner.new.generate
      @runner = File.join(PROJECT_ROOT, TEST_ROOT_NAME, 'main_spec.c')
    end

    it 'can generate runner' do
      expect(File.exist?(@runner)).to be_true
    end

    it 'main_spec.c include header' do
      expect(File.read(@runner)).to match(/#include ".*cpi\.h"/)
    end

    it 'main_spec.c include runner' do
      expect(File.read(@runner)).to match(/MPISpec_Run\( cpi_spec \)/)
      expect(File.read(@runner)).to match(/MPISpec_Run\( fabs_spec \)/)
      expect(File.read(@runner)).to match(/MPISpec_Run\( strcmp_spec \)/)
    end
  end

end
