require 'fileutils'
require 'tmpdir'
require 'spec_helper'
require 'maudsley/constants'

describe 'Configurator' do
  before :all do
    PROJECT_ROOT = Dir.mktmpdir
    @assets_path = File.join(File.dirname(__FILE__), '..', 'assets')
  end

  after :all do
    FileUtils.rm_rf(PROJECT_ROOT)
  end

  describe 'load_config' do
    context 'when project file not found' do
      it 'raise error' do
        expect(proc { Configurator.load_config }).to raise_error
      end
    end

    context 'when project file exists' do
      before do
        from = File.join(@assets_path, PROJECT_FILE_NAME)
        to   = File.join(PROJECT_ROOT, PROJECT_FILE_NAME)
        FileUtils.copy_file from, to
      end

      it 'can load project.yml' do
        expect(Configurator.load_config).not_to be_nil
      end

      it 'can load mpispec config' do
        config = (Configurator.load_config)['mpispec']
        expect(config['processes']).to eq 5
        expect(config['options']).to be_nil
      end
    end
  end
end
