# encoding: utf-8

Gem::Specification.new do |s|
  s.name        = 'maudsley'
  s.version     = '0.0.0'
  s.platform    = Gem::Platform::RUBY
  s.authors     = ['Yuu Shigetani']
  s.email       = ['s2g4t1n2@gmail.com']
  s.homepage    = 'https://github.com/calorie'
  s.summary     = %q{Maudsley is a set of tools for the automation of builds and test running for MPI}
  s.description = %q{Maudsley can deploy all of its guts into a folder. This allows it to be used without having to worry about external dependencies.}

  s.rubyforge_project = 'maudsley'

  s.add_dependency 'thor', '>= 0.14.5'
  s.add_dependency 'rake', '>= 0.8.7'

  # Files needed from submodules
  s.files       = `git ls-files`.split("\n")
  s.test_files  = `git ls-files -- spec/*`.split("\n")
  s.executables = `git ls-files -- bin/*`.split("\n").map{ |f| File.basename(f) }

  s.require_paths = ['lib']
end
